package main

import (
	"time"

	"github.com/gogf/gf/frame/g"
)

func main() {
	db := g.DB()
	db.SetDebug(true)

	t1, _ := time.Parse("2006-01-02 15:04:05", "2020-11-28 17:24:38")
	// t2, _ := time.Parse("2006-01-02 15:04:05", "2020-11-28 17:24:38")
	u, err := g.DB().Table("example").Where("action_time=?", t1).One()
	//u, err := g.DB().Table("orders").Where("updated_at>? and updated_at<?", gtime.New("2020-10-27 19:03:32"), gtime.New("2020-10-27 19:03:34")).One()
	//u, err := g.DB().Table("orders").Fields("id").Where("updated_at>'2020-10-27 19:03:32' and updated_at<'2020-10-27 19:03:34'").Value()
	g.Dump(u, err)
}
